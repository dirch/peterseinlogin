<?php

    require_once('includes/database.inc.php');
    require_once('includes/session.inc.php');
    require_once('includes/classes.inc.php');

    $db = new DBConnection();
    $db->open();
    
    $ph = new PageHandler($db);    

//    print "TEST: ".$ph->session->get_id()."<br>";
//    print "METHOD: ".$ph->method."<br>";

    if ($ph->method == 2) {
//	print "POST";
//    	print "LOGIN: ".$ph->login."<br>";
	
	if (Tools::check_password($db,$ph->login,$ph->password) >0) {

	    $ph->session->generate_id();

	    $logins = Tools::check_active_sessions($db,$ph->login,$GLOBAL_SESSION_TIMEOUT);

	    if ($logins == 0) {

		$ph->session->setValue('S_USERNAME',$ph->login);
		$ph->session->setValue('S_LOGINTIME',time());
		Tools::save_session($db,$ph->login,$ph->session->get_id(),$_SERVER['REMOTE_ADDR']);
	
		Tools::write_audit_log($db,$ph->session->get_id(),$ph->login,$_SERVER['REMOTE_ADDR'],"Successful login.",$_SERVER['PHP_SELF']);
	
		$ph->session->setValue('S_ADMIN',Tools::get_admin($db,$ph->login));
	
		header("Location: /peterseinlogin/menu.php");
		return;

	    } else {

		print "User bereits angemeldet!";

	    }


	} else {
	    print "Login failed<br>";
	}

    }

?>
<html>
    <head>
    </head>
    <body>
	<h1>Login</h1>
	<br>
	<form method="POST">
	    <table>
		<tr>
		    <td>Login:</td><td><input type="TEXT" name="login"></td>
		</tr>
		<tr>
		    <td>Password:</td><td><input type="PASSWORD" name="password"></td>
		</tr>
		<tr>
		    <td colspan="2" style="text-align: center"><input type="Submit" value="Login"></td>
		</tr>
	    </table>
	</form>
    </body>
</html>