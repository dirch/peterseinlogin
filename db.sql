--
-- PostgreSQL database dump
--

-- Dumped from database version 11.14
-- Dumped by pg_dump version 13.8

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


SET default_tablespace = '';

--
-- Name: t_active_sessions; Type: TABLE; Schema: public; Owner: ploginuser
--

CREATE TABLE public.t_active_sessions (
    id integer NOT NULL,
    sid character varying(50),
    username character varying(50),
    created timestamp without time zone DEFAULT now(),
    remoteip inet,
    lastupdate timestamp without time zone
);


ALTER TABLE public.t_active_sessions OWNER TO ploginuser;

--
-- Name: t_active_sessions_id_seq; Type: SEQUENCE; Schema: public; Owner: ploginuser
--

CREATE SEQUENCE public.t_active_sessions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_active_sessions_id_seq OWNER TO ploginuser;

--
-- Name: t_active_sessions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ploginuser
--

ALTER SEQUENCE public.t_active_sessions_id_seq OWNED BY public.t_active_sessions.id;


--
-- Name: t_audit_log; Type: TABLE; Schema: public; Owner: ploginuser
--

CREATE TABLE public.t_audit_log (
    id integer NOT NULL,
    eventdate timestamp without time zone DEFAULT now(),
    sid character varying(50),
    username character varying(100),
    description text,
    remoteip inet,
    path character varying(255)
);


ALTER TABLE public.t_audit_log OWNER TO ploginuser;

--
-- Name: t_audit_log_id_seq; Type: SEQUENCE; Schema: public; Owner: ploginuser
--

CREATE SEQUENCE public.t_audit_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_audit_log_id_seq OWNER TO ploginuser;

--
-- Name: t_audit_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ploginuser
--

ALTER SEQUENCE public.t_audit_log_id_seq OWNED BY public.t_audit_log.id;


--
-- Name: t_users; Type: TABLE; Schema: public; Owner: ploginuser
--

CREATE TABLE public.t_users (
    id integer NOT NULL,
    login character varying(50),
    firstname character varying(50),
    lastname character varying(50),
    password character varying(512),
    enabled integer DEFAULT 0,
    blocked integer DEFAULT 1,
    admin integer DEFAULT 0
);


ALTER TABLE public.t_users OWNER TO ploginuser;

--
-- Name: t_users_id_seq; Type: SEQUENCE; Schema: public; Owner: ploginuser
--

CREATE SEQUENCE public.t_users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_users_id_seq OWNER TO ploginuser;

--
-- Name: t_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ploginuser
--

ALTER SEQUENCE public.t_users_id_seq OWNED BY public.t_users.id;


--
-- Name: t_active_sessions id; Type: DEFAULT; Schema: public; Owner: ploginuser
--

ALTER TABLE ONLY public.t_active_sessions ALTER COLUMN id SET DEFAULT nextval('public.t_active_sessions_id_seq'::regclass);


--
-- Name: t_audit_log id; Type: DEFAULT; Schema: public; Owner: ploginuser
--

ALTER TABLE ONLY public.t_audit_log ALTER COLUMN id SET DEFAULT nextval('public.t_audit_log_id_seq'::regclass);


--
-- Name: t_users id; Type: DEFAULT; Schema: public; Owner: ploginuser
--

ALTER TABLE ONLY public.t_users ALTER COLUMN id SET DEFAULT nextval('public.t_users_id_seq'::regclass);

--
-- Data for Name: t_users; Type: TABLE DATA; Schema: public; Owner: ploginuser
--

COPY public.t_users (id, login, firstname, lastname, password, enabled, blocked, admin) FROM stdin;
1	Homer	Homer	Simpson	\\xdaef4953b9783365cad6615223720506cc46c5167cd16ab500fa597aa08ff964eb24fb19687f34d7665f778fcb6c5358fc0a5b81e1662cf90f73a2671c53f991	1	0	0
3	Bob			\\xdaef4953b9783365cad6615223720506cc46c5167cd16ab500fa597aa08ff964eb24fb19687f34d7665f778fcb6c5358fc0a5b81e1662cf90f73a2671c53f991	1	0	0
2	peter			\\xdaef4953b9783365cad6615223720506cc46c5167cd16ab500fa597aa08ff964eb24fb19687f34d7665f778fcb6c5358fc0a5b81e1662cf90f73a2671c53f991	1	0	1
\.


--
-- Name: t_active_sessions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ploginuser
--

SELECT pg_catalog.setval('public.t_active_sessions_id_seq', 38, true);


--
-- Name: t_audit_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ploginuser
--

SELECT pg_catalog.setval('public.t_audit_log_id_seq', 26, true);


--
-- Name: t_users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ploginuser
--

SELECT pg_catalog.setval('public.t_users_id_seq', 3, true);


--
-- PostgreSQL database dump complete
--

