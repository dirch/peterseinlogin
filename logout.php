<?php

    require_once('includes/database.inc.php');
    require_once('includes/session.inc.php');
    require_once('includes/classes.inc.php');

    $db = new DBConnection();
    $db->open();

    $ph = new PageHandler($db);

    $sid = $ph->session->get_id();
    $login = $ph->session->getValue('S_USERNAME');

    $ph->logout($db);

    Tools::write_audit_log($db,$sid,$login,$_SERVER['REMOTE_ADDR'],"Successful logout.",$_SERVER['PHP_SELF']);

    header("Location: /peterseinlogin/index.php");
?>
