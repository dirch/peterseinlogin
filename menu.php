<?php

    require_once('includes/database.inc.php');
    require_once('includes/session.inc.php');
    require_once('includes/classes.inc.php');

    $db = new DBConnection();
    $db->open();

    $ph = new PageHandler($db);
    $token = Tools::sha512($_SERVER['REMOTE_ADDR'],$GLOBAL_SALT,$GLOBAL_ITERATIONS);

    $drop_session = $_GET['drop'];
    $rtoken = $_GET['token'];
    $username = $_GET['user'];
    // Check einbauen
    
    if (strlen($drop_session)>0 && strlen($username) > 0 && strlen($rtoken) > 0) {
	if ($rtoken == $token) {
	    Tools::drop_session($db,$username,$drop_session);
	}
    }

    print "ADMIN: ".$ph->session->getValue('S_ADMIN');

?>
<html>

    <head>
    </head>
    <body>
    MENU
    <br>
    <br>
    <br>
    <a href="logout.php">LOGOUT</a>
    <br>
    <br>
    <?php
    
	$sql = "SELECT *,(lastupdate - created) as duration_last, extract(epoch from (now()-lastupdate)) as duration from t_active_sessions";
	
	$stmt = $db->handle->prepare($sql);
	$stmt->execute();
	
	$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
    ?>
    <table>
	<tr>
	    <td>Id</td><td>Session</td><td>Username</td><td>Logintime</td><td>Duration</td>
	</tr>
	<?php
	    foreach($result as $row) {
		$total_query = "?drop=".$row["sid"]."&user=".$row["username"]."&token=".$token;
		?>
		<tr>
		    <td><?php print $row["id"]?></td>
		    <td><?php print $row["sid"]?></td>
		    <td><?php print $row["username"]?></td>
		    <td><?php print $row["created"]?></td>
		    <td><?php print $row["duration"]?></td>
		    <?php if ($ph->session->getValue('S_ADMIN') > 0) { ?>
		    <td><a href="<?php print $total_query ?>">DROP</a></td>
		    <?php } ?>
		</tr>
		<?php
	    }
	?>
    </table>    
    <?php
	$result = null;
    ?>
    </body>

</html>
