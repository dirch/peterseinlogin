<?php

    class Session {

	var $name;

	function __construct() {
	    session_set_cookie_params(360,"/");
	    session_start();
	    $_SESSION['S_LAST_REQUEST'] = date();
	}

	function start() {
	    session_set_cookie_params(360,"/");
	    session_start();
	}

	function setValue($p_name,$p_value) {
	    $_SESSION[$p_name] = $p_value;
	}
	
	function getValue($p_name) {
	    return $_SESSION[$p_name];
	}

	function unset() {
	    session_unset();
	}

	function destroy() {
	    session_destroy();
	}

	function generate_id() {
	    session_regenerate_id();
	}

	function get_id() {
	    return session_id();
	}
    
	function update_counter() {
	    $_SESSION['r_counter']++;
	}

	function reset_counter() {
	    $_SESSION['r_counter']=0;
	}

	static function get() {
	    $session = new Session();
	    return $session;
	}

    }


?>