<?php

    require_once('session.inc.php');

    class PageHandler {

	var $session;
	var $methods;
	var $method;

	var $db;

	// DATA
	
	var $login;
	var $password;

	function __construct($p_db) {

	    $this->session = Session::get();	
	    $this->methods = array("UNKNOWN" => 0, "GET" => 1, "POST" => 2);
	    
	    $this->db = $p_db;
	    
	    $tmethod = $_SERVER["REQUEST_METHOD"];
	    $this->method = $this->methods[$tmethod];

	    $username = $this->session->getValue('S_USERNAME');
	    $logintime = intval($this->session->getValue('S_LOGINTIME'));
	    $lastupdate = intval($this->session->getValue('S_LASTUPDATE'));

	    print "USERNAME: ".$username."<br>";
	    print "LOGINTIME: ".$logintime."<br>";
	    
	    if (($lastupdate - $logintime) > 360) {
		$this->logout($p_db);
		header("Location: /peterseinlogin/index.php");
		
	    }
	    
	    if (strlen($username) == 0 && $logintime == 0  && $_SERVER['PHP_SELF'] != "/peterseinlogin/index.php") {
		header("Location: /peterseinlogin/index.php");
		return;
	    }

	    if ($_SERVER['PHP_SELF'] != "/peterseinlogin/index.php" && Tools::db_validate_session($p_db,$username,$this->session->get_id())== 0) {
		header("Location: /peterseinlogin/index.php");
		return;
	    }

	    if (strlen($username) > 0) {
		Tools::update_session_activity($this->db,$this->session->get_id(),$username);
		$this->session->setValue('S_LASTUPDATE',time());
	    }

	    print "SESSION: ".$this->session->get_id()."<br>";

	    if ($this->method == 2) {

		$tlogin = $_POST["login"];
		$tpassword = $_POST["password"];
		
		// Security Checks einfuegen

		$this->login = $tlogin;
		$this->password = $tpassword;

	    }

	}
	
	function logout($p_db) {
	    
	    $username = $this->session->getValue("S_USERNAME");
	    $sid = $this->session->get_id();
	    $this->session->setValue("S_USERNAME","");
	    $this->session->setValue("S_LOGINTIME",0);	    
	    $this->session->setValue("S_LASTUPDATE",0);
	    $this->session->setValue("S_ADMIN",0);

	    $this->session->unset();
	    $this->session->destroy();
	    Tools::drop_session($this->db,$username,$sid);
	}
    }

    
    class User {
    }
    
    class Tools {

/*                                          Tabelle »public.t_active_sessions«
  Spalte  |             Typ             | Sortierfolge | NULL erlaubt? |                  Vorgabewert                  
----------+-----------------------------+--------------+---------------+-----------------------------------------------
 id       | integer                     |              | not null      | nextval('t_active_sessions_id_seq'::regclass)
 sid      | character varying(50)       |              |               | 
 username | character varying(50)       |              |               | 
 created  | timestamp without time zone |              |               | now()

peterseinlogin=> 
*/
	static function update_session_activity($p_db,$p_sid,$p_login) {

	    $sql = "update t_active_sessions set lastupdate = now() where sid = :sid and username = :username";
	    
	    $stmt = $p_db->handle->prepare($sql);
	    $stmt->bindParam(':sid',$p_sid);
	    $stmt->bindParam(':username',$p_login);
	    $stmt->execute();

	    $stmt = null;

	}


	static function save_session($p_db,$p_login,$p_sid,$p_remoteip) {

	    $sql = "INSERT INTO t_active_sessions (sid,username,remoteip,lastupdate) values (:sid,:username,:ip,now())";

	    $stmt = $p_db->handle->prepare($sql);
	    $stmt->bindParam(':sid',$p_sid);
	    $stmt->bindParam(':ip',$p_remoteip);
	    $stmt->bindParam(':username',$p_login);
	    $stmt->execute();

	    $stmt = null;
	    

	}

	static function drop_session($p_db,$p_login,$p_sid) {

	    $sql = "DELETE from t_active_sessions where sid = :sid and username = :username";

	    $stmt = $p_db->handle->prepare($sql);
	    $stmt->bindParam(':sid',$p_sid);
	    $stmt->bindParam(':username',$p_login);
	    $stmt->execute();

	    $stmt = null;
	    

	}



        static function check_2fa($p_db,$p_login) {

            $rv = 0;

            $sql = "SELECT second_factor from t_users where login = :login";
            
            $stmt = $p_db->handle->prepare($sql);
            $stmt->bindParam(':login',$p_login);
            $stmt->execute();
            
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            
            foreach($result as $row) {
                $rv = $row['second_factor'];
            }
            $result = null;
            $stmt = null;
            return $rv;

        }

	static function get_admin($p_db, $p_login) {
	
	    $rv = 0;
	
	    $sql = "SELECT admin from t_users where login = :login";
	    
	    $stmt = $p_db->handle->prepare($sql);
	    $stmt->bindParam(':login',$p_login);
	    $stmt->execute();
	    
	    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
	    
	    foreach($result as $row) {
		$rv = $row["admin"];
	    }
	
	    $result = null;
	    $stmt = null;
	    return $rv;
	
	}

	static function db_validate_session($p_db, $p_login, $p_sessionid) {

	    $sql = "SELECT count(*) as cnt from t_active_sessions where username = :username and sid = :sessionid";
	    
	    $stmt = $p_db->handle->prepare($sql);
	    $stmt->bindParam(':username',$p_login);
	    $stmt->bindParam(':sessionid',$p_sessionid);
	    $stmt->execute();

	    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
	    
	    foreach($result as $row) {
		$rv = $row["cnt"];
	    }
	    $result = null;
	    $stmt = null;
	    return $rv;

	}

	static function check_active_sessions($p_db,$p_login,$p_timeout) {

	    $rv = 0;

	    $sql = "delete from t_active_sessions where extract(Epoch from (now()-lastupdate)) > :timeout";
	    $stmt = $p_db->handle->prepare($sql);
	    $stmt->bindParam(':timeout',$p_timeout);
	    $stmt->execute();
	    $stmt = null;
	    
	    $sql = "SELECT count(*) as cnt from t_active_sessions where username = :username";
	    
	    $stmt = $p_db->handle->prepare($sql);
	    $stmt->bindParam(':username',$p_login);
	    $stmt->execute();

	    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
	    
	    foreach($result as $row) {
		$rv = $row["cnt"];
	    }
	    $result = null;
	    $stmt = null;
	    return $rv;

	}

/*
peterseinlogin=> \d t_audit_log
                                            Tabelle »public.t_audit_log«
   Spalte    |             Typ             | Sortierfolge | NULL erlaubt? |               Vorgabewert               
-------------+-----------------------------+--------------+---------------+-----------------------------------------
 id          | integer                     |              | not null      | nextval('t_audit_log_id_seq'::regclass)
 eventdate   | timestamp without time zone |              |               | now()
 sid         | character varying(50)       |              |               | 
 username    | character varying(100)      |              |               | 
 description | text                        |              |               | 

peterseinlogin=> 
*/

	static function write_audit_log($p_db,$p_sid,$p_login,$p_remoteip,$p_description,$p_path) {

	    $sql = "INSERT INTO t_audit_log (sid,username,description,remoteip,path) values (:sid,:username,:description,:remoteip,:path)"; 

	    $stmt = $p_db->handle->prepare($sql);
	    $stmt->bindParam(':sid',$p_sid);
	    $stmt->bindParam(':username',$p_login);
	    $stmt->bindParam(':remoteip',$p_remoteip);
	    $stmt->bindParam(':description',$p_description);
	    $stmt->bindParam(':path',$p_path);
	    $stmt->execute();
	    
	    $stmt = null;

	}

        static function check_password($p_db,$p_login,$p_passwd) {
            
            $rv = 0;
            
            $sql = "SELECT count(*) as cnt from t_users where login = :login and password = digest(:password,'SHA512')::varchar and enabled = 1 and blocked = 0";

            $stmt = $p_db->handle->prepare($sql);
            $stmt->bindParam(':login',$p_login);
            $stmt->bindParam(':password',$p_passwd);
            $stmt->execute();
            
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            
            foreach($result as $row) {
                $rv = $row['cnt'];
            }    
            $result = null;
            $stmt = null;
            return $rv;
        }

	static function sha512($p_str, $p_salt, $p_iterations) {
    	    for ($x=0; $x<$p_iterations; $x++) {
        	$p_str = hash('sha512', $p_str . $p_salt);
    	    }
    	    return $p_str;
	}

    }


?>