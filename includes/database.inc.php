<?PHP

    require_once("settings.php");

    class DBConnection {


	var $constring;
	var $username;
	var $password;

	var $handle;

	function __construct() {

	    global $db_constring,$db_username,$db_password;

	    $this->constring = $db_constring;
	    $this->username = $db_username;
	    $this->password = $db_password;
	}

	function open() {
	    $this->handle = new PDO($this->constring,$this->username,$this->password);
	}

	function executeQuery($p_query) {
	    return $this->handle->query($p_query);
	}

	function fetchRow($p_result) {
	    return $p_result->fetch(PDO::FETCH_ASSOC);
	}

	function close() {
	    $this->handle = null;
	}

	function startTransaction() {
	    $this->handle->beginTransaction();
	}
	
	function commit() {
	    $this->handle->commit();
	}
	
	function rollback() {
	    $this->handle->rollBack();
	}

	function getNextID($seq) {
    
	    $sql = "select nextval('".$seq."') as newid";
	    $statement = $this->handle->prepare($sql);
	    $statement->execute();
	    while($result = $statement->fetch(PDO::FETCH_BOTH)) {
		$newid=$result["newid"];
	    }
	    $statement = null;
	    return $newid;	    
	}
    }
    

?>
